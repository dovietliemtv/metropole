<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;
use Domdom\Cms\Models\Allpage;

class AllpageCp extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'allpageCp Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun()
    {
        $this->page['content'] = Allpage::all()->first();
    }
}
