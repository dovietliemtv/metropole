<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;
use Domdom\Cms\Models\Contact;
use Validator;
use ValidationException;

class ContactCp extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ContactCp Component',
            'description' => 'No description provided yet...'
        ];
    }
    public function onRegister(){
        $data = post();
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ];

        $validation = Validator::make($data, $rules);
        if ( $validation->fails() ) {
            throw new ValidationException( $validation );
        }
        //create member account
        $checkContactExist = Contact::where( 'email', $data['email'] );
        if ( $checkContactExist->count() == 0 ) {
            //new member
            $contact = new Contact;
            $contact->name = $data['name'];
            $contact->email = $data['email'];
            $contact->phone = $data['phone'];
            $contact->message = $data['message'];
            if($contact->save()){
                return [
                    'success' => 'Thank you for your registration'
                ];
            }
        }
        else {
            return [
                'error' => 'Email already exists'
            ];
        }
    }

    public function defineProperties()
    {
        return [];
    }
}
