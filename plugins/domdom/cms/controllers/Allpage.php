<?php namespace Domdom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Allpage Back-end Controller
 */
class Allpage extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Domdom.Cms', 'cms', 'allpage');
    }
}
