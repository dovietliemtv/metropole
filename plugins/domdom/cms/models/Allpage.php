<?php namespace Domdom\Cms\Models;

use Model;

/**
 * allpage Model
 */
class Allpage extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_allpages';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'ourLifeStyle_title',
        'ourLifeStyle_content',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['location_note','location_point'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'map_img' => 'System\Models\File',
    ];
    public $attachMany = [
        'ourView' => 'System\Models\File',
        'ourLifeStyle_img' => 'System\Models\File'

    ];

    public function getIconOptions(){
        return[
            'museum' => 'museum',
            'convention' => 'convention',
            'operahouse' => 'operahouse',
            'vistorscenter' => 'vistorscenter',
            'sportarena' => 'sportarena',
            'sportstadium' => 'sportstadium',
            'childrenstadium' => 'childrenstadium',
            'hospital' => 'hospital',
            'marina' => 'marina',
            'resort' => 'resort',
            'park' => 'park',
            'delta' => 'delta',
            'school' => 'school',
            'facility' => 'facility',
            'cityhall' => 'cityhall',
            'metropole' => 'metropole',

        ];
    }
}
