<?php namespace Domdom\Cms\Models;

use Model;
use Backend\Models\ExportModel;
use DomDom\Cms\Models\Contact;

/**
 * ContactExport Model
 */
class ContactExport extends ExportModel
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_contact_exports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function exportData($columns, $sessionKey = null) {
        return Contact::all()->toArray();
    }

}
