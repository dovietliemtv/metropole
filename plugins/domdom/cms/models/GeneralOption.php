<?php namespace Domdom\Cms\Models;

use Model;
use Domdom\Cms\Models\Social;

/**
 * GeneralOption Model
 */
class GeneralOption extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_general_options';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
//        'name' => 'required',
    ];
    public $jsonable = ['social'];
    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'logo_desktop' => 'System\Models\File',
        'logo_mobile' => 'System\Models\File',
    ];
    public $attachMany = [];
    public function getSocialNameOptions(){
        return[
            'facebook' => 'facebook',
            'instagram' => 'instagram',
            'youtube' => 'youtube',
        ];
    }
}
