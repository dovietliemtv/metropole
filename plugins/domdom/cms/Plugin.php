<?php namespace Domdom\Cms;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'cms',
            'description' => 'No description provided yet...',
            'author'      => 'Domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register() {
        BackendMenu::registerContextSidenavPartial('Domdom.Cms', 'cms', 'plugins/domdom/cms/partials/sidebar');
    }


    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'Domdom\Cms\Components\GeneralOptionCp' => 'generaloptioncp',
            'Domdom\Cms\Components\AllpageCp' => 'allpagecp',
            'Domdom\Cms\Components\ContactCp' => 'contactcp',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate
        return [
            'domdom.cms.some_permission' => [
                'tab' => 'cms',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
//        return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'Content',
                'url'         => Backend::url('domdom/cms/allpage/update/1'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.cms.*'],
                'order'       => 500,
                'sideMenu' =>[
                    'allpage' => [
                        'label'       => 'All page',
                        'icon'        => 'icon-file-text-o',
                        'url'         => Backend::url('domdom/cms/allpage/update/1'),
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'Pages'
                    ],
                    'generaloption' => [
                        'label'       => 'General Option',
                        'icon'        => 'icon-file-text-o',
                        'url'         => Backend::url('domdom/cms/generaloption/update/1'),
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'General Option'
                    ],
                    'contact' => [
                        'label'       => 'Contact',
                        'icon'        => 'icon-file-text-o',
                        'url'         => Backend::url('domdom/cms/Contact'),
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'Contact'
                    ],

                ]
            ],
        ];
    }
}
