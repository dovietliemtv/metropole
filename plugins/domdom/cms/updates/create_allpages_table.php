<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAllpagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_allpages')){
            Schema::create('domdom_cms_allpages', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('ourStory')->nullable();
                $table->text('ourLifeStyle_title')->nullable();
                $table->text('ourLifeStyle_content')->nullable();
                $table->text('location_note')->nullable();
                $table->text('location_point')->nullable();
                $table->timestamps();
            });
        }


    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_allpages');
    }
}
