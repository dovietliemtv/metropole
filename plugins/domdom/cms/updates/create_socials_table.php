<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSocialsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_socials')){
            Schema::create('domdom_cms_socials', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('name');
                $table->text('slug');
                $table->timestamps();
            });
        }
    }
    public function down()
    {
        Schema::dropIfExists('domdom_cms_socials');
    }
}
