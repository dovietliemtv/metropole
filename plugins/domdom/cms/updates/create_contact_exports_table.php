<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactExportsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_contact_exports')){
            Schema::create('domdom_cms_contact_exports', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->timestamps();
            });
        }


    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_contact_exports');
    }
}
