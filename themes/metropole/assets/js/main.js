'use strict';
var Helpers = {
    throttle: function(func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        var now = Date.now || function() {
            return new Date().getTime();
        }
        if (!options) options = {};
        var later = function() {
            previous = options.leading === false ? 0 : now();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        };
        return function() {
            var _now = now();
            if (!previous && options.leading === false) previous = _now;
            var remaining = wait - (_now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        }
    },
    debounce: function(func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        var now = Date.now || function() {
            return new Date().getTime();
        }

        var later = function() {
            var last = now() - timestamp;

            if (last < wait && last >= 0) {
                timeout = setTimeout(later, wait - last);
            }
            else {
                timeout = null;
                if (!immediate) {
                    result = func.apply(context, args);
                    if (!timeout) { context = args = null; }
                }
            }
        }

        return function() {
            context = this;
            args = arguments;
            timestamp = now();
            var callNow = immediate && !timeout;
            if (!timeout) { timeout = setTimeout(later, wait); }
            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }

            return result;
        }
    },
    prefixMethod: function(obj, method) {
        var pfx = ['webkit', 'moz', 'ms', 'o', ''];
        var p = 0, m, t;
        while (p < pfx.length && !obj[m]) {
            m = method;
            if (pfx[p] == '') {
                m = m.substr(0,1).toLowerCase() + m.substr(1);
            }
            m = pfx[p] + m;
            t = typeof obj[m];
            if (t != 'undefined') {

                pfx = [pfx[p]];
                return (t == 'function' ? obj[m]() : obj[m]);
            }
            p++;
        }
    },
    isFullscreen: function() {
        return Helpers.prefixMethod(document, 'FullScreen') || Helpers.prefixMethod(document, 'IsFullScreen');
    },
    setFullscreen: function(el) {
        Helpers.prefixMethod(el, 'RequestFullScreen');
    },
    closeFullscreen: function() {
        return Helpers.prefixMethod(document, 'CancelFullScreen');
    }
};
(function ($) {
    $(document).ready(function () {
        let fullpage = $('#fullpage');
        let body = $('body');

       // icon menu mobile
        McButton();

        if(body.hasClass('ourstory')){
            setTimeout(function () {
                onYouTubeIframeAPIReady(); // youtube api
            }, 300);
        }

        videoAutoHeight($('#video-iframe'));
        //
        $('.js-menu').on('click', function (e) {
            e.preventDefault();
            if($('.js-menu').hasClass('active')){
                body.addClass('show-menu');
            }else{
                body.removeClass('show-menu');
            }

        });

        // js register
        $('.js-register').on('click', function (e) {
           e.preventDefault();

            $('body, html').stop().animate({scrollTop:0}, 500, 'swing');

            body.toggleClass('show-register-form');
        });

        // js viewmore
        $('.js-more').on('click', function (e) {
            e.preventDefault();
            $(this).hide();
            $('.dd-b-content').addClass('dd-viewmore');

        })
        $('.js-close').on('click', function (e) {
            e.preventDefault();
            $('.dd-b-content').removeClass('dd-viewmore');
            $('.js-more').fadeIn();
        })

        // check form
        $('#dd-register-form').isHappy({
            fields: {
                '#yourName': {
                    required: true
                },
                '#yourEmail': {
                    required: true,
                    test: happy.email // this can be *any* function that returns true, false, or an instance of Error
                },
                '#yourPhone': {
                    required: true,
                    test: happy.phoneNumber // this can be *any* function that returns true, false, or an instance of Error
                }
            }
        });


        //js close form
        $('.js-close-form').on('click', function (e) {
            e.preventDefault();
            body.removeClass('show-register-form');
        });

        $(document).mouseup(function(e)
        {
            var container = $(".dd-s-register-info .dd-s-register-info-inner");
            var containerVideoPopup = $(".dd-s-video-popup-inner");
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                body.removeClass('show-register-form');

            }
            if (!containerVideoPopup.is(e.target) && containerVideoPopup.has(e.target).length === 0)
            {
                body.removeClass('show-video-popup');
            }
        });

        //js-open-video-popup
        $('.js-open-video-popup').on('click', function (e) {
            e.preventDefault();
            body.addClass('show-video-popup');
        });
        $('.js-close-video-popup').on('click', function (e) {
            e.preventDefault();
            body.removeClass('show-video-popup');
        });


        // active menu
        if(body.hasClass('ourstory')){
            activeMenu('ourstory');
        }else if(body.hasClass('ourview')){
            activeMenu('ourview');
        }else if(body.hasClass('ourlocation')){
            activeMenu('ourlocation');
        }else if(body.hasClass('ourlifestyle')){
            activeMenu('ourlifestyle');
        }
        
        // language js
        $('.dd-b-language ul').hover(function () {
            $('.dd-b-language-option').css('opacity','1');
        }, function () {
            $('.dd-b-language-option').css('opacity','0');
        })

        // slick slide
        $('.dd-b-slider-fade').slick({
            dots: true,
            infinite: true,
            speed: 500,
            autoplay: true,
            autoplaySpeed: 6000,
            pauseOnHover:false,
            fade: true,
            cssEase: 'linear',
            prevArrow: '<div class="arrow-slider pre-nav"><span></span></div>',
            nextArrow: '<div class="arrow-slider next-nav"><span></span></div>'

        });

        // scale the map (responsive)

        var $window = $(window);
// calculate hotpot's positions for location page.
        var calcHotpotPos = function(map, checkWidth) {
            var sectionWrapper = map;
            var checkWidth = checkWidth;
            var scrollArea = map.parent();
            var sectionBg = new Image;
            sectionBg.src = sectionWrapper.attr('data-bg');
            var pxSize = parseInt(sectionWrapper.attr('data-px-ratio'));
            var originBgWidth, originBgHeight;
            var hotpots = sectionWrapper.find('.dd-hotpot');
            var curBg = map.find('.js-current-bg');
            var sectionWrapperX,sectionWrapperY, curBgWidth, curBgHeight, lastX, gapX;
            var mapRatio = curBg.width()/ $window.width();

            var updateX = function(newX, newY) {
                sectionWrapper.css({
                    'margin-left': newX + 'px',
                    'margin-top': newY + 'px'
                });
            }

            var dragBg = function(x, y) {
                curBgWidth = curBg.width();
                var winWidth = $window.width();
                var minX, maxX;

                if(curBgWidth > winWidth) {
                    minX = -winWidth / 2;
                    maxX = winWidth / 2 - curBgWidth;
                    sectionWrapperX += (x - y) * mapRatio;
                    if(sectionWrapperX >= minX) {
                        sectionWrapperX = minX;
                    } else {
                        if(sectionWrapperX <= maxX) {
                            sectionWrapperX = maxX;
                        }
                    }
                    updateX(sectionWrapperX);
                }

                // console.log((curX - lastX) * mapRatio + ' / ' + curX + ' / ' + lastX + ' / ' + mapRatio);
                lastX = x;
            }

            sectionBg.onload = function(e) {
                curBgWidth = curBg.width();
                curBgHeight = curBg.height();
                var winW = $window.width();
                if(winW > curBgWidth && checkWidth == true) {
                    var scaleRatio = winW / curBgWidth;
                    curBgHeight = curBgHeight * scaleRatio;
                    curBgWidth = winW;
                    curBg.width(curBgWidth);
                    curBg.height(curBgHeight);
                }

                sectionWrapper.width(curBgWidth);
                sectionWrapper.height(curBgHeight);
                originBgWidth = sectionBg.width;
                originBgHeight = sectionBg.height;

                var scaleX = curBgWidth * pxSize / originBgWidth;
                var scaleY = curBgHeight * pxSize / originBgHeight;
                var scale = scaleX > scaleY ? scaleX : scaleY;
                sectionWrapperX = -curBgWidth / 2;
                sectionWrapperY = -curBgHeight / 2;
                updateX(sectionWrapperX, sectionWrapperY);
                hotpots.each(function() {
                    scaleMap.call(this, scale);
                });
            }

            // touchstart or mousedown
            // var canTouch = ('ontouchstart' in document.documentElement)  ? 'yes' : 'no';
            var canTouch = $('html').hasClass('mobile');
            $window.resize(function() {
                setTimeout(function(){
                    curBg.removeAttr('style');
                    sectionWrapper.removeAttr('style');
                    curBgWidth = curBg.width();
                    curBgHeight = curBg.height();
                    var winW = $window.width();
                    if(winW > curBgWidth && checkWidth == true) {
                        var scaleRatio = winW / curBgWidth;
                        curBgHeight = curBgHeight * scaleRatio;
                        curBgWidth = winW;
                        curBg.width(curBgWidth);
                        curBg.height(curBgHeight);
                    }

                    mapRatio = curBg.width()/ $window.width();
                    sectionWrapper.width(curBgWidth);
                    sectionWrapper.height(curBgHeight);
                    var scaleX = curBgWidth * pxSize / originBgWidth;
                    var scaleY = curBgHeight * pxSize / originBgHeight;
                    var scale = scaleX > scaleY ? scaleX : scaleY;
                    sectionWrapperX = -curBgWidth / 2;
                    sectionWrapperY = -curBgHeight / 2;
                    updateX(sectionWrapperX, sectionWrapperY);
                    hotpots.each(function() {
                        scaleMap.call(this, scale);
                    });
                },100);
            });
            // drag the map
            if(canTouch) {
                scrollArea.on({
                    'touchstart': function(e) {
                        lastX = e.changedTouches[0].pageX;
                        // $.fn.fullpage.setAutoScrolling(false);
                    },
                    'touchmove': function(e) {
                        var curX = e.changedTouches[0].pageX;
                        dragBg(curX, lastX);
                    },
                    'touchend': function(e) {
                        // $.fn.fullpage.setAutoScrolling(true);
                    }
                });
            } else {
                sectionWrapper.draggable({
                    axis: "x",
                    start: function(e) {
                        lastX = e.pageX;
                    },
                    drag: function(e) {
                        var curX = e.pageX;
                        dragBg(curX, lastX);
                    },
                    stop: function() {}
                });
            }


        }

// hotspot

        var scaleMap = function(scale) {
            var $this = $(this);
            var xy = $this.attr('data-xy').split(' ');
            var transX = parseFloat(xy[0]);
            var transY = parseFloat(xy[1]);
            var offsetX = $this.width() - $window.width();
            var offsetY = $this.height() - $window.height();
            if (offsetX > 0) {
                transX += (-offsetX / 2) / scale;
            }
            $this.css({
                'transform': 'scale(' + scale + ') translate( ' + transX + 'px,' + transY + 'px)',
                '-webkit-transform': 'scale(' + scale + ') translate( ' + transX + 'px,' + transY + 'px)',
                'moz-transform': 'scale(' + scale + ') translate( ' + transX + 'px,' + transY + 'px)',
            });
        }
        var hotspot = function () {

            var sectionWrapper = $('.dd-location-map');
            var hotpots = $('.dd-hotspot');
            var sectionBg = new Image;
            var imgRealWidth, imgRealHeight, curWidth, curHeight, mapRatio;
            var $window = $(window);
            sectionBg.onload = function (e) {
                 imgRealWidth = sectionBg.width;
                 imgRealHeight = sectionBg.height;
                 curWidth = $('.dd-location-map img').width();
                 curHeight = $('.dd-location-map img').height();
                $('.dd-b-hotspot').css({
                    'width': curWidth
                });

                mapRatio = curWidth/ imgRealWidth;
                $('.dd-b-hotspot').width(curWidth);
                $('.dd-b-hotspot').height(curHeight);
                var scaleX = curWidth  / imgRealWidth;
                var scaleY = curHeight  / imgRealHeight;
                var scale = scaleX > scaleY ? scaleX : scaleY;
                hotpots.each(function() {
                    scaleMap.call(this, scale);
                });

            }
            sectionBg.src = sectionWrapper.attr('data-bg');



        }
        if(body.hasClass('ourlocation')){
            hotspot();
        }
        //slider
        let sliderFade = $('.dd-b-slider-fade1');
        let sliderFadeinit = () => {
            sliderFade.slick({
                dots: true,
                autoplay: true,
                autoplaySpeed: 6000,
                infinite: true,
                speed: 500,
                arrows: false,
                fade: true,
                cssEase: 'linear'
            });
        }
            // detect screen size
        body.layoutController({
            onSmartphone: function(){
                if($('.dd-b-slider-fade1.slick-initialized').length) {
                    sliderFade.slick('unslick');
                }
                sliderFadeinit();

            },
            onTablet: function(){
                if($('.dd-b-slider-fade1.slick-initialized').length) {
                    sliderFade.slick('unslick');
                }
                sliderFadeinit();
            },
            onDesktop: function(){
                if($('.dd-b-slider-fade1.slick-initialized').length) {
                    sliderFade.slick('unslick');
                }
            }
        });



        // mobileBrag($('.dd-b-zoom'), false);

        $(window).resize(function () {
            setTimeout(function () {
                videoAutoHeight($('#video-iframe'));
                if(body.hasClass('ourlocation')){
                    hotspot();
                }
            }, 100);
        });

    });//end document ready


function McButton() {
    // icon menu
    var McButton = $("[data=hamburger-menu]");
    var McBar1 = McButton.find("b:nth-child(1)");
    var McBar2 = McButton.find("b:nth-child(2)");
    var McBar3 = McButton.find("b:nth-child(3)");
    McButton.click( function() {
        $(this).toggleClass("active");
        if (McButton.hasClass("active")) {
            McBar1.velocity({ top: "50%" }, {duration: 0, easing: "swing"});
            McBar3.velocity({ top: "58%" }, {duration: 0, easing: "swing"})
                .velocity({translateY:"-50%",rotateZ:"90deg"}, {duration: 400, delay: 0, easing: [300,20] });
            McButton.velocity({translateY:"-50%",rotateZ:"135deg"}, {duration: 400, delay: 0, easing: [300,20] });
        } else {
            // McButton.velocity("reverse");
            McButton.velocity({translateY:"-50%",rotateZ:"0"}, {duration: 400, delay: 0, easing: [300,20] });

            McBar3.velocity({translateY:"-50%",rotateZ:"0deg"}, {duration: 400, easing: [300,20] })
                .velocity({top: "100%" }, {duration: 0, easing: "swing"});
            McBar1.velocity("reverse", {delay: 400});
        }
    });
}


function videoAutoWidth(iframe) {

    var heightVideo = iframe.height();
    var widthVideo = heightVideo * 16 /9;

    iframe.css({
        'width': widthVideo
    });
}
function videoAutoHeight(iframe) {
    var widthVideo = iframe.width();
    var heightVideo = widthVideo * 9 /16;
    iframe.css({
        'height': heightVideo
    });
}

function activeMenu(bodyClass) {
    $('.dd-' + bodyClass).addClass('active');
}

    //youtube api
    var player, playing = false;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('video-iframe', {
                videoId: $('#dd-youtubeId').val(),
                playerVars: {
                    autoplay: 1,
                    showinfo: 0,
                    rel : 0,
                    loop: 0,
                    controls: 1,
                    autohide: 1,
                    wmode: 'opaque',
                    events: {
                        'onReady': onPlayerReady,
                        // 'onStateChange': onPlayerStateChange
                    }
                }
        });

    }
    function onPlayerReady(event) {
            event.target.playVideo();
        //     event.target.mute();
        //     player.setLoop(true);
    }



})(jQuery);
function contactResponse(data) {
    if(data.success){
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-head').fadeOut();
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body form').fadeOut();
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body .dd-confirm').text(data.success);
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body .dd-confirm').fadeIn();
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body').css('padding','100px 0');

    }
    if(data.error){
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-head').fadeOut();
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body form').fadeOut();
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body .dd-confirm').addClass('red-color');
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body .dd-confirm').text(data.error);
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body .dd-confirm').fadeIn();
        $('.dd-s-register-info .dd-s-register-info-inner .dd-b-container .dd-b-body').css('padding','100px 0');

    }
}
