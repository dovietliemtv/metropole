-- MySQL dump 10.13  Distrib 5.6.38, for Linux (x86_64)
--
-- Host: localhost    Database: metropole
-- ------------------------------------------------------
-- Server version	5.6.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backend_access_log`
--

DROP TABLE IF EXISTS `backend_access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_access_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_access_log`
--

LOCK TABLES `backend_access_log` WRITE;
/*!40000 ALTER TABLE `backend_access_log` DISABLE KEYS */;
INSERT INTO `backend_access_log` VALUES (1,1,'118.69.34.252','2018-06-28 19:26:17','2018-06-28 19:26:17'),(2,1,'118.69.34.252','2018-06-28 22:13:44','2018-06-28 22:13:44'),(3,1,'115.76.1.58','2018-07-07 23:37:11','2018-07-07 23:37:11');
/*!40000 ALTER TABLE `backend_access_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backend_user_groups`
--

DROP TABLE IF EXISTS `backend_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_user_groups`
--

LOCK TABLES `backend_user_groups` WRITE;
/*!40000 ALTER TABLE `backend_user_groups` DISABLE KEYS */;
INSERT INTO `backend_user_groups` VALUES (1,'Owners','2018-06-28 19:24:19','2018-06-28 19:24:19','owners','Default group for website owners.',0);
/*!40000 ALTER TABLE `backend_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backend_user_preferences`
--

DROP TABLE IF EXISTS `backend_user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_user_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_user_preferences`
--

LOCK TABLES `backend_user_preferences` WRITE;
/*!40000 ALTER TABLE `backend_user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `backend_user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backend_user_roles`
--

DROP TABLE IF EXISTS `backend_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_unique` (`name`),
  KEY `role_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_user_roles`
--

LOCK TABLES `backend_user_roles` WRITE;
/*!40000 ALTER TABLE `backend_user_roles` DISABLE KEYS */;
INSERT INTO `backend_user_roles` VALUES (1,'Publisher','publisher','Site editor with access to publishing tools.','',1,'2018-06-28 19:24:19','2018-06-28 19:24:19'),(2,'Developer','developer','Site administrator with access to developer tools.','',1,'2018-06-28 19:24:19','2018-06-28 19:24:19');
/*!40000 ALTER TABLE `backend_user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backend_user_throttle`
--

DROP TABLE IF EXISTS `backend_user_throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `backend_user_throttle_user_id_index` (`user_id`),
  KEY `backend_user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_user_throttle`
--

LOCK TABLES `backend_user_throttle` WRITE;
/*!40000 ALTER TABLE `backend_user_throttle` DISABLE KEYS */;
INSERT INTO `backend_user_throttle` VALUES (1,1,'118.69.34.252',0,NULL,0,NULL,0,NULL),(2,1,'115.76.1.58',0,NULL,0,NULL,0,NULL);
/*!40000 ALTER TABLE `backend_user_throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backend_users`
--

DROP TABLE IF EXISTS `backend_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `act_code_index` (`activation_code`),
  KEY `reset_code_index` (`reset_password_code`),
  KEY `admin_role_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_users`
--

LOCK TABLES `backend_users` WRITE;
/*!40000 ALTER TABLE `backend_users` DISABLE KEYS */;
INSERT INTO `backend_users` VALUES (1,'Admin','Person','admin','admin@domain.tld','$2y$10$3c63my1BsF59WSR3ckhemeWL0l3WRYuoUm041UtCixO.SRbN9XX.6',NULL,'$2y$10$8zNLEnhPHcwJ/.YhKxGZmu8lWUqLicAjnVSUXLzySv5ZqYMSkx0ke',NULL,'',1,2,NULL,'2018-07-07 23:37:11','2018-06-28 19:24:19','2018-07-07 23:37:11',1);
/*!40000 ALTER TABLE `backend_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backend_users_groups`
--

DROP TABLE IF EXISTS `backend_users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backend_users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_users_groups`
--

LOCK TABLES `backend_users_groups` WRITE;
/*!40000 ALTER TABLE `backend_users_groups` DISABLE KEYS */;
INSERT INTO `backend_users_groups` VALUES (1,1);
/*!40000 ALTER TABLE `backend_users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache`
--

LOCK TABLES `cache` WRITE;
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_theme_data`
--

DROP TABLE IF EXISTS `cms_theme_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_theme_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_data_theme_index` (`theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_theme_data`
--

LOCK TABLES `cms_theme_data` WRITE;
/*!40000 ALTER TABLE `cms_theme_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_theme_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_theme_logs`
--

DROP TABLE IF EXISTS `cms_theme_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_theme_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_logs_type_index` (`type`),
  KEY `cms_theme_logs_theme_index` (`theme`),
  KEY `cms_theme_logs_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_theme_logs`
--

LOCK TABLES `cms_theme_logs` WRITE;
/*!40000 ALTER TABLE `cms_theme_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_theme_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deferred_bindings`
--

DROP TABLE IF EXISTS `deferred_bindings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deferred_bindings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deferred_bindings_master_type_index` (`master_type`),
  KEY `deferred_bindings_master_field_index` (`master_field`),
  KEY `deferred_bindings_slave_type_index` (`slave_type`),
  KEY `deferred_bindings_slave_id_index` (`slave_id`),
  KEY `deferred_bindings_session_key_index` (`session_key`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deferred_bindings`
--

LOCK TABLES `deferred_bindings` WRITE;
/*!40000 ALTER TABLE `deferred_bindings` DISABLE KEYS */;
INSERT INTO `deferred_bindings` VALUES (13,'Domdom\\Cms\\Models\\Allpage','map_img','System\\Models\\File','14','79XTojnuoFH0TBGeo2sqN0kQ6S0X7ebv1TkUURiX',0,'2018-06-28 20:39:03','2018-06-28 20:39:03'),(14,'Domdom\\Cms\\Models\\Allpage','map_img','System\\Models\\File','15','79XTojnuoFH0TBGeo2sqN0kQ6S0X7ebv1TkUURiX',0,'2018-06-28 21:00:43','2018-06-28 21:00:43'),(15,'Domdom\\Cms\\Models\\Allpage','map_img','System\\Models\\File','16','0UgQjv6Ch9hDZLRz2p6iaLHJU0k4rsBJvOL9K4aV',0,'2018-07-07 23:37:24','2018-07-07 23:37:24'),(16,'Domdom\\Cms\\Models\\GaneralOption','logo_desktop','System\\Models\\File','1','38VWsjLj4GyOuzlEDCwTS7l1z2bFG39WIf1JBAob',0,'2018-07-07 23:37:41','2018-07-07 23:37:41'),(19,'Domdom\\Cms\\Models\\Allpage','map_img','System\\Models\\File','17','WeCEvFeicBYKyxV40pN42A8cJ68kviNJYrrn74pA',0,'2018-07-08 00:08:10','2018-07-08 00:08:10'),(20,'Domdom\\Cms\\Models\\Allpage','map_img','System\\Models\\File','19','WeCEvFeicBYKyxV40pN42A8cJ68kviNJYrrn74pA',0,'2018-07-08 00:16:06','2018-07-08 00:16:06');
/*!40000 ALTER TABLE `deferred_bindings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domdom_cms_allpages`
--

DROP TABLE IF EXISTS `domdom_cms_allpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domdom_cms_allpages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ourStory` text COLLATE utf8mb4_unicode_ci,
  `ourLifeStyle_title` text COLLATE utf8mb4_unicode_ci,
  `ourLifeStyle_content` text COLLATE utf8mb4_unicode_ci,
  `location_note` text COLLATE utf8mb4_unicode_ci,
  `location_point` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domdom_cms_allpages`
--

LOCK TABLES `domdom_cms_allpages` WRITE;
/*!40000 ALTER TABLE `domdom_cms_allpages` DISABLE KEYS */;
INSERT INTO `domdom_cms_allpages` VALUES (1,'rE5BnnmUWxI','THE METROPOLE THU THIEM – NEW METROPOLIS OF SAIGON','<p>Located in the core area of Thu Thiem peninsular, where the walking streets will lead you to all the new city landmarks within few easy steps, The Metropole possesses 180 sweeping view toward Saigon river and District 1 center. Colorful and bustling shopping streets, exciting cultural and artistic events, green riverside spaces and modern transportation systems, here are all of what you desire for a future metropolis. That\'s how the name comes – The Metropole Thu Thiem, New Metropolis of Saigon.</p>','[{\"name\":\"Museum\",\"icon\":\"museum\"},{\"name\":\"Convention Center\",\"icon\":\"convention\"},{\"name\":\"Opera House\",\"icon\":\"operahouse\"},{\"name\":\"Visitors Center\",\"icon\":\"vistorscenter\"},{\"name\":\"Sport Arena\",\"icon\":\"sportarena\"},{\"name\":\"Sport Stadium\",\"icon\":\"sportstadium\"},{\"name\":\"Children\'s Museum\",\"icon\":\"childrenstadium\"},{\"name\":\"International Hospital\",\"icon\":\"\"},{\"name\":\"Marina Complex\",\"icon\":\"marina\"},{\"name\":\"Southern Delta Resort\",\"icon\":\"resort\"},{\"name\":\"Aquatic Park\",\"icon\":\"park\"},{\"name\":\"Southern Delta Reserch\",\"icon\":\"delta\"},{\"name\":\"School\",\"icon\":\"school\"},{\"name\":\"Cutural Facility\",\"icon\":\"facility\"},{\"name\":\"City Hall\",\"icon\":\"cityhall\"}]','[{\"name\":\"METROPOLE\",\"icon\":\"metropole\",\"posx\":\"-172\",\"posy\":\"120\"},{\"name\":\"Museum\",\"icon\":\"museum\",\"posx\":\"-123\",\"posy\":\"60\"},{\"name\":\"Convention Center\",\"icon\":\"convention\",\"posx\":\"-205\",\"posy\":\"92\"},{\"name\":\"Opera House\",\"icon\":\"operahouse\",\"posx\":\"-240\",\"posy\":\"132\"},{\"name\":\"Visitors Center\",\"icon\":\"vistorscenter\",\"posx\":\"-217\",\"posy\":\"177\"},{\"name\":\"Sport Arena\",\"icon\":\"sportarena\",\"posx\":\"-158\",\"posy\":\"280\"},{\"name\":\"Sport Stadium\",\"icon\":\"sportstadium\",\"posx\":\"-140\",\"posy\":\"335\"},{\"name\":\"Children\'s Museum\",\"icon\":\"childrenstadium\",\"posx\":\"-78\",\"posy\":\"190\"},{\"name\":\"International Hospital\",\"icon\":\"hospital\",\"posx\":\"28\",\"posy\":\"124\"},{\"name\":\"Marina Complex\",\"icon\":\"marina\",\"posx\":\"70\",\"posy\":\"138\"},{\"name\":\"Southern Delta Resort\",\"icon\":\"resort\",\"posx\":\"-90\",\"posy\":\"337\"},{\"name\":\"Aquatic Park\",\"icon\":\"park\",\"posx\":\"-3\",\"posy\":\"337\"},{\"name\":\"Southern Delta Reserch\",\"icon\":\"delta\",\"posx\":\"0\",\"posy\":\"270\"},{\"name\":\"Southern Delta Reserch\",\"icon\":\"delta\",\"posx\":\"-48\",\"posy\":\"264\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"-107\",\"posy\":\"292\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"-48\",\"posy\":\"302px\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"-213\",\"posy\":\"232px\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"-97\",\"posy\":\"117\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"-34\",\"posy\":\"104\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"115\",\"posy\":\"28\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"120\",\"posy\":\"71\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"7\",\"posy\":\"200\"},{\"name\":\"School\",\"icon\":\"school\",\"posx\":\"-25\",\"posy\":\"219\"},{\"name\":\"Cutural Facility\",\"icon\":\"facility\",\"posx\":\"-61\",\"posy\":\"94\"},{\"name\":\"Cutural Facility\",\"icon\":\"facility\",\"posx\":\"87\",\"posy\":\"78\"},{\"name\":\"City Hall\",\"icon\":\"cityhall\",\"posx\":\"-63\",\"posy\":\"166\"}]','2018-06-28 19:38:06','2018-07-08 03:38:29');
/*!40000 ALTER TABLE `domdom_cms_allpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domdom_cms_contacts`
--

DROP TABLE IF EXISTS `domdom_cms_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domdom_cms_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domdom_cms_contacts`
--

LOCK TABLES `domdom_cms_contacts` WRITE;
/*!40000 ALTER TABLE `domdom_cms_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `domdom_cms_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domdom_cms_general_options`
--

DROP TABLE IF EXISTS `domdom_cms_general_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domdom_cms_general_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `social` text COLLATE utf8mb4_unicode_ci,
  `policy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domdom_cms_general_options`
--

LOCK TABLES `domdom_cms_general_options` WRITE;
/*!40000 ALTER TABLE `domdom_cms_general_options` DISABLE KEYS */;
INSERT INTO `domdom_cms_general_options` VALUES (1,'[{\"social_name\":\"facebook\",\"social_link\":\"https:\\/\\/www.facebook.com\\/themetropolethuthiem\\/\"},{\"social_name\":\"instagram\",\"social_link\":\"https:\\/\\/www.instagram.com\\/themetropolethuthiem_official\\/\"},{\"social_name\":\"youtube\",\"social_link\":\"https:\\/\\/www.youtube.com\\/channel\\/UC7iQ60lGkcnKPeVLnPLyxuA\\/featured?view_as=subscriber\"}]','© 2018 All Rights Reserved.','2018-06-28 19:29:33','2018-06-28 19:29:33');
/*!40000 ALTER TABLE `domdom_cms_general_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domdom_cms_homepages`
--

DROP TABLE IF EXISTS `domdom_cms_homepages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domdom_cms_homepages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_top_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_top` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domdom_cms_homepages`
--

LOCK TABLES `domdom_cms_homepages` WRITE;
/*!40000 ALTER TABLE `domdom_cms_homepages` DISABLE KEYS */;
INSERT INTO `domdom_cms_homepages` VALUES (1,'rE5BnnmUWxI','The Metropole Thu Thiem - New Metropolis of Saigon','<p>Located in the core area of Thu Thiem peninsular, where the walking streets will lead you to all the new city landmarks within few easy steps, The Metropole possesses 180 sweeping view toward Saigon river and District 1 center. Colorful and bustling shopping streets, exciting cultural and artistic events, green riverside spaces and modern transportation systems, here are all of what you desire for a future metropolis. That\'s how the name comes – The Metropole Thu Thiem, New Metropolis of Saigon.</p>','[{\"feature_image\":\"\\/imagehome\\/img1.jpg\",\"image_title\":\"TWO LADY SMILLING FACELIT HOVER\",\"image_des\":\"This is short text description only for two lady smiling facelit very bahahhaha. Please, don\\u2019t try this at home. and just 3 line.\"},{\"feature_image\":\"\\/imagehome\\/img2.jpg\",\"image_title\":\"TWO LADY SMILLING FACELIT HOVER\",\"image_des\":\"This is short text description only for two lady smiling facelit very bahahhaha. Please, don\\u2019t try this at home. and just 3 line.\"},{\"feature_image\":\"\\/imagehome\\/img3.jpg\",\"image_title\":\"TWO LADY SMILLING FACELIT HOVER\",\"image_des\":\"This is short text description only for two lady smiling facelit very bahahhaha. Please, don\\u2019t try this at home. and just 3 line.\"},{\"feature_image\":\"\\/imagehome\\/img4.jpg\",\"image_title\":\"TWO LADY SMILLING FACELIT HOVER\",\"image_des\":\"This is short text description only for two lady smiling facelit very bahahhaha. Please, don\\u2019t try this at home. and just 3 line.\"}]','2018-06-18 03:03:18','2018-06-18 19:22:31');
/*!40000 ALTER TABLE `domdom_cms_homepages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domdom_cms_socials`
--

DROP TABLE IF EXISTS `domdom_cms_socials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domdom_cms_socials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domdom_cms_socials`
--

LOCK TABLES `domdom_cms_socials` WRITE;
/*!40000 ALTER TABLE `domdom_cms_socials` DISABLE KEYS */;
/*!40000 ALTER TABLE `domdom_cms_socials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2013_10_01_000001_Db_Deferred_Bindings',1),(2,'2013_10_01_000002_Db_System_Files',1),(3,'2013_10_01_000003_Db_System_Plugin_Versions',1),(4,'2013_10_01_000004_Db_System_Plugin_History',1),(5,'2013_10_01_000005_Db_System_Settings',1),(6,'2013_10_01_000006_Db_System_Parameters',1),(7,'2013_10_01_000007_Db_System_Add_Disabled_Flag',1),(8,'2013_10_01_000008_Db_System_Mail_Templates',1),(9,'2013_10_01_000009_Db_System_Mail_Layouts',1),(10,'2014_10_01_000010_Db_Jobs',1),(11,'2014_10_01_000011_Db_System_Event_Logs',1),(12,'2014_10_01_000012_Db_System_Request_Logs',1),(13,'2014_10_01_000013_Db_System_Sessions',1),(14,'2015_10_01_000014_Db_System_Mail_Layout_Rename',1),(15,'2015_10_01_000015_Db_System_Add_Frozen_Flag',1),(16,'2015_10_01_000016_Db_Cache',1),(17,'2015_10_01_000017_Db_System_Revisions',1),(18,'2015_10_01_000018_Db_FailedJobs',1),(19,'2016_10_01_000019_Db_System_Plugin_History_Detail_Text',1),(20,'2016_10_01_000020_Db_System_Timestamp_Fix',1),(21,'2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session',1),(22,'2017_10_01_000021_Db_System_Sessions_Update',1),(23,'2017_10_01_000022_Db_Jobs_FailedJobs_Update',1),(24,'2017_10_01_000023_Db_System_Mail_Partials',1),(25,'2013_10_01_000001_Db_Backend_Users',2),(26,'2013_10_01_000002_Db_Backend_User_Groups',2),(27,'2013_10_01_000003_Db_Backend_Users_Groups',2),(28,'2013_10_01_000004_Db_Backend_User_Throttle',2),(29,'2014_01_04_000005_Db_Backend_User_Preferences',2),(30,'2014_10_01_000006_Db_Backend_Access_Log',2),(31,'2014_10_01_000007_Db_Backend_Add_Description_Field',2),(32,'2015_10_01_000008_Db_Backend_Add_Superuser_Flag',2),(33,'2016_10_01_000009_Db_Backend_Timestamp_Fix',2),(34,'2017_10_01_000010_Db_Backend_User_Roles',2),(35,'2014_10_01_000001_Db_Cms_Theme_Data',3),(36,'2016_10_01_000002_Db_Cms_Timestamp_Fix',3),(37,'2017_10_01_000003_Db_Cms_Theme_Logs',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rainlab_translate_attributes`
--

DROP TABLE IF EXISTS `rainlab_translate_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_data` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `rainlab_translate_attributes_locale_index` (`locale`),
  KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  KEY `rainlab_translate_attributes_model_type_index` (`model_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rainlab_translate_attributes`
--

LOCK TABLES `rainlab_translate_attributes` WRITE;
/*!40000 ALTER TABLE `rainlab_translate_attributes` DISABLE KEYS */;
INSERT INTO `rainlab_translate_attributes` VALUES (1,'vi','1','Domdom\\Cms\\Models\\Allpage','{\"ourLifeStyle_title\":\"THE METROPOLE THỦ THIÊM - NEW METROPOLIS OF SAIGON\",\"ourLifeStyle_content\":\"<p>Tọa lạc ở khu vực lõi trung tâm của bán đảo Thủ Thiêm, nơi bạn có thể đi bộ đến tất cả các công trình biểu tượng mới của thành phố, The Metropole sở hữu tầm nhìn bao quát 180 độ về phía sông Sài Gòn và trung tâm Quận 1. Những con phố mua sắm đa sắc màu, những sự kiện văn hóa và nghệ thuật đặc sắc, không gian xanh mát ven sông, hệ thống giao thông hiện đại..., đây là tất cả những gì bạn khao khát ở một thành phố của tương lai. Cũng bởi thế, dự án được mang tên The Metropole Thủ Thiêm - New Metropolis of Saigon.<\\/p>\\r\\n\"}');
/*!40000 ALTER TABLE `rainlab_translate_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rainlab_translate_indexes`
--

DROP TABLE IF EXISTS `rainlab_translate_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `rainlab_translate_indexes_locale_index` (`locale`),
  KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  KEY `rainlab_translate_indexes_item_index` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rainlab_translate_indexes`
--

LOCK TABLES `rainlab_translate_indexes` WRITE;
/*!40000 ALTER TABLE `rainlab_translate_indexes` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_translate_indexes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rainlab_translate_locales`
--

DROP TABLE IF EXISTS `rainlab_translate_locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rainlab_translate_locales_code_index` (`code`),
  KEY `rainlab_translate_locales_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rainlab_translate_locales`
--

LOCK TABLES `rainlab_translate_locales` WRITE;
/*!40000 ALTER TABLE `rainlab_translate_locales` DISABLE KEYS */;
INSERT INTO `rainlab_translate_locales` VALUES (1,'en','English',1,1,1),(2,'vi','Vietnamese',0,1,2);
/*!40000 ALTER TABLE `rainlab_translate_locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rainlab_translate_messages`
--

DROP TABLE IF EXISTS `rainlab_translate_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_data` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `rainlab_translate_messages_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rainlab_translate_messages`
--

LOCK TABLES `rainlab_translate_messages` WRITE;
/*!40000 ALTER TABLE `rainlab_translate_messages` DISABLE KEYS */;
INSERT INTO `rainlab_translate_messages` VALUES (1,'register.now','{\"x\":\"REGISTER NOW\",\"vi\":\"\\u0110\\u0103ng k\\u00fd\"}'),(2,'our.story','{\"x\":\"Our Story\",\"vi\":\"C\\u00e2u Chuy\\u1ec7n\"}'),(3,'our.lifestyle','{\"x\":\"Our Lifestyle\",\"vi\":\"Phong c\\u00e1ch\"}'),(4,'our.view','{\"x\":\"Our View\",\"vi\":\"T\\u1ea7m nh\\u00ecn\"}'),(5,'our.location','{\"x\":\"Our Location\",\"vi\":\"V\\u1ecb tr\\u00ed\"}'),(6,'share','{\"x\":\"Share\",\"vi\":\"Chia s\\u1ebd\"}'),(7,'follow.us.on','{\"x\":\"FOLLOW US ON\",\"vi\":\"Theo d\\u00f5i\"}'),(8,'','{\"x\":null}'),(9,'register.info','{\"x\":\"Register info\",\"vi\":\"Th\\u00f4ng tin \\u0111\\u0103ng k\\u00fd\"}'),(10,'please.fill.out.the.form.below.and.we.will.contact.you.shortly','{\"x\":\"Please fill out the form below and we will contact\\n                    you shortly.\"}'),(11,'thank.you.for.your.registration','{\"x\":\"Thank you for your registration\"}'),(12,'your.name','{\"x\":\"Your name * \",\"vi\":\"H\\u1ecd v\\u00e0 T\\u00ean*\"}'),(13,'your.email','{\"x\":\"Your email* \",\"vi\":\"\\u0110\\u1ecba ch\\u1ec9 email *\"}'),(14,'your.phone.number','{\"x\":\"Your phone number* \",\"vi\":\"S\\u1ed1 \\u0111i\\u1ec7n tho\\u1ea1i\"}'),(15,'your.message','{\"x\":\"Your message \",\"vi\":\"N\\u1ed9i dung\"}'),(16,'sent','{\"x\":\"Sent\",\"vi\":\"G\\u1eedi\"}'),(17,'close','{\"x\":\"Close\",\"vi\":\"\\u0110\\u00f3ng\"}'),(18,'2018.all.rights.reserved','{\"x\":\"\\u00a9 2018 All Rights Reserved.\"}'),(19,'read.more','{\"x\":\"Read more\"}'),(20,'legend','{\"x\":\"Legend\"}'),(21,'museum','{\"x\":\"Museum\"}'),(22,'metropole','{\"x\":\"METROPOLE\"}'),(23,'opera.house','{\"x\":\"Opera House\"}'),(24,'visitors.center','{\"x\":\"Visitors Center\"}'),(25,'sport.arena','{\"x\":\"Sport Arena\"}'),(26,'sport.stadium','{\"x\":\"Sport Stadium\"}'),(27,'convention.center','{\"x\":\"Convention Center\"}'),(28,'childrens.museum','{\"x\":\"Children\'s Museum\"}'),(29,'international.hospital','{\"x\":\"International Hospital\"}'),(30,'marina.complex','{\"x\":\"Marina Complex\"}'),(31,'southern.delta.resort','{\"x\":\"Southern Delta Resort\"}'),(32,'aquatic.park','{\"x\":\"Aquatic Park\"}'),(33,'southern.delta.reserch','{\"x\":\"Southern Delta Reserch\"}'),(34,'school','{\"x\":\"School\"}'),(35,'cutural.facility','{\"x\":\"Cutural Facility\"}'),(36,'city.hall','{\"x\":\"City Hall\"}'),(37,'follow.us','{\"x\":\"Follow Us\"}'),(38,'copyright.2018','{\"x\":\"Copyright 2018\"}');
/*!40000 ALTER TABLE `rainlab_translate_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_event_logs`
--

DROP TABLE IF EXISTS `system_event_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_event_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_event_logs_level_index` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_event_logs`
--

LOCK TABLES `system_event_logs` WRITE;
/*!40000 ALTER TABLE `system_event_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_event_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_files`
--

DROP TABLE IF EXISTS `system_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_files_field_index` (`field`),
  KEY `system_files_attachment_id_index` (`attachment_id`),
  KEY `system_files_attachment_type_index` (`attachment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_files`
--

LOCK TABLES `system_files` WRITE;
/*!40000 ALTER TABLE `system_files` DISABLE KEYS */;
INSERT INTO `system_files` VALUES (2,'5b3598febdcbd934022583.svg','logo-mobile.svg',25736,'image/svg+xml',NULL,NULL,'logo_mobile','1','Domdom\\Cms\\Models\\GaneralOption',1,2,'2018-06-28 19:27:10','2018-06-28 19:29:33'),(3,'5b359a8a39d5a285952700.jpg','img2.jpg',575561,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,3,'2018-06-28 19:33:46','2018-06-28 19:38:06'),(4,'5b359a8a61cd2274869559.jpg','img1.jpg',743308,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,4,'2018-06-28 19:33:46','2018-06-28 19:38:06'),(5,'5b359a8b4a0ce666398453.jpg','img3.jpg',773308,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,5,'2018-06-28 19:33:47','2018-06-28 19:38:06'),(6,'5b359a8bbd222102076953.jpg','img4.jpg',1149271,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,6,'2018-06-28 19:33:47','2018-06-28 19:38:06'),(7,'5b359a9224263253157768.jpg','img2.jpg',575561,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,7,'2018-06-28 19:33:54','2018-06-28 19:38:06'),(8,'5b359a9242590728171978.jpg','img1.jpg',743308,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,8,'2018-06-28 19:33:54','2018-06-28 19:38:06'),(9,'5b359a935ea43971789041.jpg','img3.jpg',773308,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,9,'2018-06-28 19:33:55','2018-06-28 19:38:06'),(10,'5b359b1432680628220715.jpg','location.jpg',692723,'image/jpeg',NULL,NULL,'ourView','1','Domdom\\Cms\\Models\\Allpage',1,10,'2018-06-28 19:36:04','2018-06-28 19:38:06'),(13,'5b359bdae1f1c383102359.jpg','feature.jpg',239485,'image/jpeg',NULL,NULL,'ourLifeStyle_img','1','Domdom\\Cms\\Models\\Allpage',1,13,'2018-06-28 19:39:22','2018-06-28 19:39:22'),(18,'5b41b137b079c168298568.png','logo.png',12718,'image/png',NULL,NULL,'logo_desktop','1','Domdom\\Cms\\Models\\GaneralOption',1,18,'2018-07-07 23:37:43','2018-07-07 23:37:43'),(20,'5b41ba39c787d536556546.jpg','map1.jpg',293086,'image/jpeg',NULL,NULL,'map_img','1','Domdom\\Cms\\Models\\Allpage',1,20,'2018-07-08 00:16:09','2018-07-08 00:16:09');
/*!40000 ALTER TABLE `system_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_mail_layouts`
--

DROP TABLE IF EXISTS `system_mail_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_mail_layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_mail_layouts`
--

LOCK TABLES `system_mail_layouts` WRITE;
/*!40000 ALTER TABLE `system_mail_layouts` DISABLE KEYS */;
INSERT INTO `system_mail_layouts` VALUES (1,'Default layout','default','<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>','{{ content|raw }}','@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}',1,'2018-06-28 19:24:19','2018-06-28 19:24:19'),(2,'System layout','system','<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>','{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.','@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}',1,'2018-06-28 19:24:19','2018-06-28 19:24:19');
/*!40000 ALTER TABLE `system_mail_layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_mail_partials`
--

DROP TABLE IF EXISTS `system_mail_partials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_mail_partials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_mail_partials`
--

LOCK TABLES `system_mail_partials` WRITE;
/*!40000 ALTER TABLE `system_mail_partials` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_partials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_mail_templates`
--

DROP TABLE IF EXISTS `system_mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_mail_templates_layout_id_index` (`layout_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_mail_templates`
--

LOCK TABLES `system_mail_templates` WRITE;
/*!40000 ALTER TABLE `system_mail_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_parameters`
--

DROP TABLE IF EXISTS `system_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_parameters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `item_index` (`namespace`,`group`,`item`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_parameters`
--

LOCK TABLES `system_parameters` WRITE;
/*!40000 ALTER TABLE `system_parameters` DISABLE KEYS */;
INSERT INTO `system_parameters` VALUES (1,'system','update','count','0'),(2,'system','update','retry','1531118237'),(3,'cms','theme','active','\"metropole\"');
/*!40000 ALTER TABLE `system_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_plugin_history`
--

DROP TABLE IF EXISTS `system_plugin_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_plugin_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_plugin_history_code_index` (`code`),
  KEY `system_plugin_history_type_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_plugin_history`
--

LOCK TABLES `system_plugin_history` WRITE;
/*!40000 ALTER TABLE `system_plugin_history` DISABLE KEYS */;
INSERT INTO `system_plugin_history` VALUES (1,'October.Demo','comment','1.0.1','First version of Demo','2018-06-28 19:24:13'),(2,'Domdom.Cms','script','1.0.4','create_contacts_table.php','2018-06-28 19:24:14'),(3,'Domdom.Cms','script','1.0.4','create_ganeral_options_table.php','2018-06-28 19:24:14'),(4,'Domdom.Cms','script','1.0.4','create_socials_table.php','2018-06-28 19:24:14'),(5,'Domdom.Cms','script','1.0.4','create_allpages_table.php','2018-06-28 19:24:14'),(6,'Domdom.Cms','comment','1.0.4','First version of cms','2018-06-28 19:24:14'),(7,'RainLab.Translate','script','1.0.1','create_messages_table.php','2018-06-28 19:24:14'),(8,'RainLab.Translate','script','1.0.1','create_attributes_table.php','2018-06-28 19:24:15'),(9,'RainLab.Translate','script','1.0.1','create_locales_table.php','2018-06-28 19:24:15'),(10,'RainLab.Translate','comment','1.0.1','First version of Translate','2018-06-28 19:24:15'),(11,'RainLab.Translate','comment','1.0.2','Languages and Messages can now be deleted.','2018-06-28 19:24:16'),(12,'RainLab.Translate','comment','1.0.3','Minor updates for latest October release.','2018-06-28 19:24:16'),(13,'RainLab.Translate','comment','1.0.4','Locale cache will clear when updating a language.','2018-06-28 19:24:16'),(14,'RainLab.Translate','comment','1.0.5','Add Spanish language and fix plugin config.','2018-06-28 19:24:16'),(15,'RainLab.Translate','comment','1.0.6','Minor improvements to the code.','2018-06-28 19:24:16'),(16,'RainLab.Translate','comment','1.0.7','Fixes major bug where translations are skipped entirely!','2018-06-28 19:24:16'),(17,'RainLab.Translate','comment','1.0.8','Minor bug fixes.','2018-06-28 19:24:16'),(18,'RainLab.Translate','comment','1.0.9','Fixes an issue where newly created models lose their translated values.','2018-06-28 19:24:16'),(19,'RainLab.Translate','comment','1.0.10','Minor fix for latest build.','2018-06-28 19:24:16'),(20,'RainLab.Translate','comment','1.0.11','Fix multilingual rich editor when used in stretch mode.','2018-06-28 19:24:16'),(21,'RainLab.Translate','comment','1.1.0','Introduce compatibility with RainLab.Pages plugin.','2018-06-28 19:24:16'),(22,'RainLab.Translate','comment','1.1.1','Minor UI fix to the language picker.','2018-06-28 19:24:16'),(23,'RainLab.Translate','comment','1.1.2','Add support for translating Static Content files.','2018-06-28 19:24:16'),(24,'RainLab.Translate','comment','1.1.3','Improved support for the multilingual rich editor.','2018-06-28 19:24:16'),(25,'RainLab.Translate','comment','1.1.4','Adds new multilingual markdown editor.','2018-06-28 19:24:16'),(26,'RainLab.Translate','comment','1.1.5','Minor update to the multilingual control API.','2018-06-28 19:24:16'),(27,'RainLab.Translate','comment','1.1.6','Minor improvements in the message editor.','2018-06-28 19:24:16'),(28,'RainLab.Translate','comment','1.1.7','Fixes bug not showing content when first loading multilingual textarea controls.','2018-06-28 19:24:16'),(29,'RainLab.Translate','comment','1.2.0','CMS pages now support translating the URL.','2018-06-28 19:24:16'),(30,'RainLab.Translate','comment','1.2.1','Minor update in the rich editor and code editor language control position.','2018-06-28 19:24:16'),(31,'RainLab.Translate','comment','1.2.2','Static Pages now support translating the URL.','2018-06-28 19:24:16'),(32,'RainLab.Translate','comment','1.2.3','Fixes Rich Editor when inserting a page link.','2018-06-28 19:24:16'),(33,'RainLab.Translate','script','1.2.4','create_indexes_table.php','2018-06-28 19:24:17'),(34,'RainLab.Translate','comment','1.2.4','Translatable attributes can now be declared as indexes.','2018-06-28 19:24:17'),(35,'RainLab.Translate','comment','1.2.5','Adds new multilingual repeater form widget.','2018-06-28 19:24:17'),(36,'RainLab.Translate','comment','1.2.6','Fixes repeater usage with static pages plugin.','2018-06-28 19:24:17'),(37,'RainLab.Translate','comment','1.2.7','Fixes placeholder usage with static pages plugin.','2018-06-28 19:24:17'),(38,'RainLab.Translate','comment','1.2.8','Improvements to code for latest October build compatibility.','2018-06-28 19:24:17'),(39,'RainLab.Translate','comment','1.2.9','Fixes context for translated strings when used with Static Pages.','2018-06-28 19:24:17'),(40,'RainLab.Translate','comment','1.2.10','Minor UI fix to the multilingual repeater.','2018-06-28 19:24:17'),(41,'RainLab.Translate','comment','1.2.11','Fixes translation not working with partials loaded via AJAX.','2018-06-28 19:24:17'),(42,'RainLab.Translate','comment','1.2.12','Add support for translating the new grouped repeater feature.','2018-06-28 19:24:18'),(43,'RainLab.Translate','comment','1.3.0','Added search to the translate messages page.','2018-06-28 19:24:18'),(44,'RainLab.Translate','script','1.3.1','builder_table_update_rainlab_translate_locales.php','2018-06-28 19:24:18'),(45,'RainLab.Translate','script','1.3.1','seed_all_tables.php','2018-06-28 19:24:18'),(46,'RainLab.Translate','comment','1.3.1','Added reordering to languages','2018-06-28 19:24:18'),(47,'RainLab.Translate','comment','1.3.2','Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.','2018-06-28 19:24:18'),(48,'RainLab.Translate','comment','1.3.3','Fix to the locale picker session handling in Build 420 onwards.','2018-06-28 19:24:18'),(49,'RainLab.Translate','comment','1.3.4','Add alternate hreflang elements and adds prefixDefaultLocale setting.','2018-06-28 19:24:18'),(50,'RainLab.Translate','comment','1.3.5','Fix MLRepeater bug when switching locales.','2018-06-28 19:24:18'),(51,'RainLab.Translate','comment','1.3.6','Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4','2018-06-28 19:24:18'),(52,'RainLab.Translate','comment','1.3.7','Fix config reference in LocaleMiddleware','2018-06-28 19:24:18'),(53,'RainLab.Translate','comment','1.3.8','Keep query string when switching locales','2018-06-28 19:24:18'),(54,'LaminSanneh.FlexiContact','comment','1.0.0','First Version with basic functionality','2018-06-28 19:24:18'),(55,'LaminSanneh.FlexiContact','comment','1.0.1','Fixed email subject to send actual subject set in backend','2018-06-28 19:24:18'),(56,'LaminSanneh.FlexiContact','comment','1.0.2','Added validation to contact form fields','2018-06-28 19:24:18'),(57,'LaminSanneh.FlexiContact','comment','1.0.3','Changed body input field type from text to textarea','2018-06-28 19:24:18'),(58,'LaminSanneh.FlexiContact','comment','1.0.4','Updated default component markup to use more appropriate looking twitter bootstrap classes','2018-06-28 19:24:18'),(59,'LaminSanneh.FlexiContact','comment','1.0.5','Corrected spelling for Marketing on the backend settings','2018-06-28 19:24:18'),(60,'LaminSanneh.FlexiContact','script','1.0.6','Add proper validation message outputting','2018-06-28 19:24:18'),(61,'LaminSanneh.FlexiContact','script','1.0.6','Added option to include or exclude main script file','2018-06-28 19:24:19'),(62,'LaminSanneh.FlexiContact','comment','1.0.6','Added ability to include bootstrap or not on component config','2018-06-28 19:24:19'),(63,'LaminSanneh.FlexiContact','script','1.0.7','Updated readme file','2018-06-28 19:24:19'),(64,'LaminSanneh.FlexiContact','comment','1.0.7','Updated contact component default markup file','2018-06-28 19:24:19'),(65,'LaminSanneh.FlexiContact','comment','1.1.0','Mail template is now registered properly','2018-06-28 19:24:19'),(66,'LaminSanneh.FlexiContact','comment','1.2.0','Add Proper validation that can be localized','2018-06-28 19:24:19'),(67,'LaminSanneh.FlexiContact','comment','1.2.1','Added permissions to the settings page. PR by @matissjanis','2018-06-28 19:24:19'),(68,'LaminSanneh.FlexiContact','comment','1.2.2','Added polish language features','2018-06-28 19:24:19'),(69,'LaminSanneh.FlexiContact','comment','1.2.3','Modified mail templatedefault text','2018-06-28 19:24:19'),(70,'LaminSanneh.FlexiContact','comment','1.3.0','!!! Added captcha feature, which requires valid google captcha credentials to work','2018-06-28 19:24:19'),(71,'LaminSanneh.FlexiContact','comment','1.3.1','Set replyTo instead of from-header when sending','2018-06-28 19:24:19'),(72,'LaminSanneh.FlexiContact','comment','1.3.2','Added german translation language file','2018-06-28 19:24:19'),(73,'LaminSanneh.FlexiContact','comment','1.3.3','Added option to allow user to enable or disable captcha in contact form','2018-06-28 19:24:19'),(74,'LaminSanneh.FlexiContact','comment','1.3.4','Made sure captcha enabling or disabling doesnt produce bug','2018-06-28 19:24:19');
/*!40000 ALTER TABLE `system_plugin_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_plugin_versions`
--

DROP TABLE IF EXISTS `system_plugin_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_plugin_versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_plugin_versions_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_plugin_versions`
--

LOCK TABLES `system_plugin_versions` WRITE;
/*!40000 ALTER TABLE `system_plugin_versions` DISABLE KEYS */;
INSERT INTO `system_plugin_versions` VALUES (1,'October.Demo','1.0.1','2018-06-28 19:24:13',0,0),(2,'Domdom.Cms','1.0.4','2018-06-28 19:24:14',0,0),(3,'RainLab.Translate','1.3.8','2018-06-28 19:24:18',0,0),(4,'LaminSanneh.FlexiContact','1.3.4','2018-06-28 19:24:19',0,0);
/*!40000 ALTER TABLE `system_plugin_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_request_logs`
--

DROP TABLE IF EXISTS `system_request_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_request_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_request_logs`
--

LOCK TABLES `system_request_logs` WRITE;
/*!40000 ALTER TABLE `system_request_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_request_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_revisions`
--

DROP TABLE IF EXISTS `system_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  KEY `system_revisions_user_id_index` (`user_id`),
  KEY `system_revisions_field_index` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_revisions`
--

LOCK TABLES `system_revisions` WRITE;
/*!40000 ALTER TABLE `system_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `system_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `system_settings_item_index` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_settings`
--

LOCK TABLES `system_settings` WRITE;
/*!40000 ALTER TABLE `system_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-11  8:30:10

